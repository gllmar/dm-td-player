# dm-td-player

## [Distributed memories](https://gitlab.com/gllmar/distributed-memories) touchdesigner player


### dm-td-player

`dm-td-player/dm-td-player.toe`

![dm-td-player](docs/dm_td_player_v20211121.png)


### dm-td-tester

`pd/dm-td-player-tester.pd`

![dm-td-player](docs/dm_td_tester_v20211121.png)